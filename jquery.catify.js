/*!
 * CATIFY - Lemme see dem kitties
 * Original author: Zach Stowell (@ZachStowell)
 * Licensed under the "Do whatever the hell you want to" License
 */

;(function ( $, window, document, undefined ) {
    //Shamelessly stolen from the Modernizr Source.
    function canPlayAudio(){
        var elem = document.createElement('audio'),
            bool = false;

        try { 
            if ( bool = !!elem.canPlayType ) {
                bool      = new Boolean(bool);
                bool.ogg  = elem.canPlayType('audio/ogg; codecs="vorbis"');
                bool.mp3  = elem.canPlayType('audio/mpeg;');

                // Mimetypes accepted:
                //   https://developer.mozilla.org/En/Media_formats_supported_by_the_audio_and_video_elements
                //   http://bit.ly/iphoneoscodecs
                bool.wav  = elem.canPlayType('audio/wav; codecs="1"');
                bool.m4a  = elem.canPlayType('audio/x-m4a;') || elem.canPlayType('audio/aac;');
            }
        } catch(e) { }
        
        return bool;

    }


    $.fn.catify = function(options){
        
        //Defaults
        var defaults = {
            trigger: 'click', //click, timer, konami-code
            delay: 2500 //this is the amount of time we wait on the timer before OMG KITTEHS
        };

        //Allow for the options to be used
        var options = $.extend(defaults, options);

        //The Big Mamma-jamma
        return this.each(function(){
            var _this = $(this);

            //Kittehs!
            var catImage = '<img id="omg-kitteh" style="display:none" src="kitteh.png" />'


        });


    }

})(jQuery, window, document);